import { Candidate } from './candidate';
import { Status } from './status';
export class Card {
    id: string;
    title: string;
    columnId: Status;
    boardId: string;
    order: number;
    candidate: Candidate;
}
