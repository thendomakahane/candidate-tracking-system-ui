export class Rating {
    id: number;
    logicalSkills: number;
    communicationSkills: number;
    honesty: number;
    passion: number;
    abilityToLearn: number;
    flexibility: number;
    organisationalFit: number;
}
