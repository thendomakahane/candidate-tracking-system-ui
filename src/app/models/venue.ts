export class Venue {
    id: number;
    address: string;
    name: string;
}
