import { Candidate } from './candidate';
export class Assessment {
    id: number;
    dateGiven: Date;
    dateSubmitted: Date;
    comments: string;
    passed: boolean;
    candidate: Candidate;
    name: string;
}
