import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Card } from '../models/card';
import { Candidate } from '../models/candidate';
import { LookUp } from '../utilities/lookup';
import { Observable } from 'rxjs';
import { AlertService } from '../services/alert.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    // tslint:disable-next-line:object-literal-key-quotes
    'Authorization':  `${window.btoa('mtn:mtn')}`
  })
};

@Injectable({
  providedIn: 'root'
})
export class CandidateService {
  BASE_URL = 'http://localhost:8080/';
  lookUp =  new LookUp();
  constructor(private http: HttpClient, private alertService: AlertService) {

  }

  getCards(): Card[] {
    const cards = [];
    const candidates = [];
    this.http.get<any[]>(this.BASE_URL + 'candidate').subscribe(data => {
      data.forEach(ca => {
        const candidate = {
          email: ca.email,
          gender: ca.gender,
          id: ca.id,
          names: ca.names,
          phoneNumber: ca.phoneNumber,
          role: ca.role,
          status: ca.status,
          surname: ca.surname,
          idNumber: ca.idNumber,
          assessmentCompleted: ca.assessmentCompleted,
          evaluationCompleted: ca.evaluationCompleted,
          onboardingCompleted: ca.onboardingCompleted,
          placementCompleted: ca.placementCompleted
        } as Candidate;

        candidates.push(candidate);
      });
      if (candidates.length > 0) {
        // tslint:disable-next-line:prefer-for-of
        for (let index = 0; index < candidates.length; index++) {
          const card = {
            boardId: 'candidate-b',
            candidate: candidates[index],
            columnId: this.lookUp.lookUpStatus(candidates[index].status.name),
            id: candidates[index].id.toString(),
            order: index,
            title: candidates[index].names + ' ' + candidates[index].surname
          } as Card;
          cards.push(card);
        }
        console.log('cards--->', cards);
        return cards;
      }
    }, err => {
      this.alertService.error('Unable to retrieve candidates, please try again later');
      return cards;
    });
    return cards;
  }

  addCandidate(candidate: any): Observable<any> {
    return this.http.post(this.BASE_URL + 'candidate', JSON.stringify(candidate), httpOptions);
  }

  getCandidateById(candidateId): Observable<any> {
    return this.http.get(this.BASE_URL + `candidate/${candidateId}`);
  }

  updateCandidate(candidate: any): Observable<any> {
    return this.http.put(this.BASE_URL + 'candidate' , JSON.stringify(candidate), httpOptions);
  }

  addAppraisal(appraisal: any): Observable<any> {
    return this.http.post(this.BASE_URL + 'assessment' , JSON.stringify(appraisal), httpOptions);
  }

  getAppraisalbyCandidate(candidateId): Observable<any> {
    return this.http.get(this.BASE_URL + `assessment/candidate/${candidateId}`, httpOptions);
  }

  addInterview(interview): Observable<any> {
    return this.http.post(this.BASE_URL + 'interview', interview, httpOptions);
  }

  addEvaluation(evaluation): Observable<any> {
    return this.http.post(this.BASE_URL + 'evaluation', evaluation, httpOptions);
  }

  addOnboarding(onboarding): Observable<any> {
    return this.http.post(this.BASE_URL + 'onboarding', onboarding, httpOptions);
  }

  addPlacement(placement): Observable<any> {
    return this.http.post(this.BASE_URL + 'placement', placement, httpOptions);
  }

  getAllRoles(): Observable<any[]> {
    return this.http.get<any[]>(this.BASE_URL + 'role');
  }

  getAllStatus(): Observable<any[]> {
    return this.http.get<any[]>(this.BASE_URL + 'status');
  }

  getCandidatesBySearch(search: any): Observable<any[]> {
    return this.http.post<any[]>(this.BASE_URL + 'search/candidate', search, httpOptions);
  }

  getAllVenues(): Observable<any[]> {
    return this.http.get<any[]>(this.BASE_URL + 'venue');
  }

  getAllUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.BASE_URL + 'user');
  }

  getAllUsersCycles(): Observable<any[]> {
    return this.http.get<any[]>('http://nlaeblrvq13:15000/api/v1/cycles/', httpOptions);
  }

  getEvaluationByInterview(interviewId): Observable<any[]> {
    return this.http.get<any[]>(this.BASE_URL + `evaluation/interview/${interviewId}`);
  }

  getInterviewByCandidate(candidateId): Observable<any> {
    return this.http.get<any>(this.BASE_URL + `interview/candidate/${candidateId}`);
  }

  getOnboardingByCandidate(candidateId): Observable<any> {
    return this.http.get<any>(this.BASE_URL + `onboarding/candidate/${candidateId}`);
  }

  getPlacementByCandidate(candidateId): Observable<any> {
    return this.http.get<any>(this.BASE_URL + `placement/candidate/${candidateId}`);
  }

  sendMeetingRequestEmail(interview): Observable<any> {
    return this.http.post<any>(this.BASE_URL + 'email', interview, httpOptions);
  }

  getBillingCycles(): Observable<any[]> {
    return this.http.get<any[]>('http://nlaeblrvq13:15000/api/v1/cycles/', httpOptions);
  }

}
