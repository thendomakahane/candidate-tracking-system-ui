export class Onboarding {
    id: number;
    references = false;
    verification = false;
    offer = false;
    comment = '';
    candidate: any;
}
