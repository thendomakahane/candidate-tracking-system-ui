import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModalConfig } from '../models/modal-config';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-custom-modal',
  templateUrl: './custom-modal.component.html',
  styleUrls: ['./custom-modal.component.css']
})
export class CustomModalComponent implements OnInit {

  @Input() modalConfig: ModalConfig;
  @Output() button1Action: EventEmitter<any>;
  @Input() roles: any[];
  @Input() statuses: any[];
  @Output() button2Action: EventEmitter<any>;
  @Output() save: EventEmitter<any>;
  modalForm: FormGroup;
  constructor(config: NgbModalConfig) {
    this.button1Action = new EventEmitter();
    this.button2Action = new EventEmitter();
    this.save =  new EventEmitter();
    config.backdrop = 'static';
  }

  ngOnInit() {
    console.log('modal init', this.modalConfig);
  }

  onButton1Action() {
    this.button1Action.emit();
    console.log('event emitted');
  }

  onButton2Action() {
    this.button2Action.emit(this.modalConfig.formData);
    console.log('event emitted');
  }

  isValid(): boolean {
    let result = true;
    this.modalConfig.formData.forEach(element => {
      if (element.required === true && (element.model === '' || element.model === null || element.model === undefined)) {
        result = false;
      }
    });
    return result;
  }

}
