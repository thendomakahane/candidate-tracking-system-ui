import { Component, OnInit } from '@angular/core';
import { Search } from '../models/search';
import { CandidateService } from '../services/candidate.service';
import { PagerService } from '../services/pager.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  search: Search;
  roles: any[];
  statuses: any[];
  candidates: any[];
  page = 1;
  pageSize = 6;
  collection = 0;
  showLoader = false;
  pager: any = {};

  pagedItems: any[];

  firstnameDisabled = false;
  surnameDisabled = false;
  idNumberDisabled = false;
  roleDisabled = false;
  statusDisabled = false;

  constructor(private candidateService: CandidateService, private pagerService: PagerService) {
    this.search = new Search();
    this.candidates = [];
    document.getElementById('nav-width').style.width = '100%';
   }

  ngOnInit() {
    this.candidateService.getAllRoles().subscribe(data => {
      this.roles = data;
    });

    this.candidateService.getAllStatus().subscribe(data => {
      this.statuses = data;
    });
  }

  onSearch() {
    this.showLoader = true;
    this.candidateService.getCandidatesBySearch(JSON.stringify(this.search)).subscribe(data => {
      this.showLoader = false;
      if (data) {
        this.candidates = data;
        this.collection = this.candidates.length;
        this.setPage(1);
      } else {
        this.candidates = [];
        this.pagedItems = [];
      }
    }, error => {
      this.showLoader = false;
      console.log(error);
    });
  }

  onClear() {
    this.search = new Search();
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.candidates.length, page);

    // get current page of items
    this.pagedItems = this.candidates.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

  firstnameChanged(val) {
    if (val) {
      this.surnameDisabled = true;
      this.idNumberDisabled = true;
      this.roleDisabled = true;
      this.statusDisabled = true;
    } else {
      this.surnameDisabled = false;
      this.idNumberDisabled = false;
      this.roleDisabled = false;
      this.statusDisabled = false;
    }
  }

  surnameChanged(val) {
    if (val) {
      this.firstnameDisabled = true;
      this.idNumberDisabled = true;
      this.roleDisabled = true;
      this.statusDisabled = true;
    } else {
      this.firstnameDisabled = false;
      this.idNumberDisabled = false;
      this.roleDisabled = false;
      this.statusDisabled = false;
    }
  }

  idNumberChanged(val) {
    if (val) {
      this.firstnameDisabled = true;
      this.surnameDisabled = true;
      this.roleDisabled = true;
      this.statusDisabled = true;
    } else {
      this.firstnameDisabled = false;
      this.surnameDisabled = false;
      this.roleDisabled = false;
      this.statusDisabled = false;
    }
  }

  roleChanged(val) {
    if (val) {
      this.firstnameDisabled = true;
      this.surnameDisabled = true;
      this.idNumberDisabled = true;
      this.statusDisabled = true;
    } else {
      this.firstnameDisabled = false;
      this.surnameDisabled = false;
      this.idNumberDisabled = false;
      this.statusDisabled = false;
    }
  }

  statusChanged(val) {
    if (val) {
      this.firstnameDisabled = true;
      this.surnameDisabled = true;
      this.roleDisabled = true;
      this.roleDisabled = true;
    } else {
      this.firstnameDisabled = false;
      this.surnameDisabled = false;
      this.roleDisabled = false;
      this.roleDisabled = false;
    }
  }

}
