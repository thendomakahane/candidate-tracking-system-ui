import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RoleService } from '../services/role.service';
import { Alert } from '../models/Alert';
import { Role } from '../models/Role';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  roleForm: FormGroup;
  alert: Alert;
  showAlert = false;
  roles: Role[];
  role: Role;
  cols: any;
  displayDialog;
  roleNameMinLength = 3;

  constructor(private roleService: RoleService) { }

  ngOnInit() {
    this.roleService.getAllRoles().subscribe(data => {
      console.log('*** role ***');
      console.log(data);
      this.roles = data;
    });
    this.roleForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(this.roleNameMinLength)]),
    });
    this.cols = [{
      field: 'name', header: 'Name',
    }];
  }

  get name() {
    return this.roleForm.get('name');
  }

  clearAll() {
    this.roleForm.reset();
  }

  onSubmit() {
    this.showAlert = true;
    console.log(this.roleForm.value);
    this.roleService.saveRole(this.roleForm.value).subscribe(data => {
      this.alert = {
        type: 'success',
        message: 'SUCCESS: role saved!',
      };
    }, err => {
      this.alert = {
        type: 'danger',
        message: 'ERROR: could not save role!',
      };
      console.error(err);
    });
  }

  close() {
    this.showAlert = false;
  }

  showDialogToAdd() {
    this.role = {
      id: 0,
      name: ''
    };
    this.displayDialog = true;
  }
}
