import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Role } from '../models/Role';
import { Observable } from 'rxjs';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  BASE_URL = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  saveRole(role: Role): Observable<any> {
    return this.http.post(this.BASE_URL + 'role', JSON.stringify(role), HTTP_OPTIONS);
  }

  getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.BASE_URL + 'role', HTTP_OPTIONS);
  }
}
