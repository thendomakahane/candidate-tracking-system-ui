import { Card } from './card';
import { Column } from './column';
export class Board {
    id: string;
    title: string;
    columns: Column[];
    cards: Card[];
}
