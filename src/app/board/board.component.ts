import { Component, Input, OnInit, OnDestroy, AfterViewInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Board } from '../models/board';
import { Column } from '../models/column';
import { Card } from '../models/card';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { BoardService } from '../services/board.service';
import { CandidateService } from '../services/candidate.service';

declare var jQuery: any;
let curYPos = 0;
let curXPos = 0;
let curDown = false;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {
  boards: Board[];
  board: Board;
  addingColumn = false;
  addColumnText: string;
  editingTilte = false;
  currentTitle: string;
  boardWidth: number;
  columnsAdded = 0;
  roles: any[];
  statuses: any[];
  venues: any[];
  users: any[];
  ratings: any[];
  subscription: Subscription;

  constructor(public el: ElementRef, private router: Router, private route: ActivatedRoute, private boardService: BoardService,
              private candidateService: CandidateService, private ref: ChangeDetectorRef) {
    this.subscription = this.candidateService.getAllRoles().subscribe(data => {
      this.roles = data;
    }, error => {
      console.log(error);
    });

    this.subscription = this.candidateService.getAllStatus().subscribe(data => {
      this.statuses = data;
    }, error => {
      console.log(error);
    });

    this.getAllUsers();
    this.getAllVenues();

    this.candidateService.getAllUsersCycles().subscribe(data => {
      alert(JSON.stringify(data));
    }, error => {
      alert(JSON.stringify(error));
    })
   }

  ngOnInit() {
    this.boards = this.boardService.getAllBoards();
    if (this.boards.length > 0) {
      this.board = this.boards[0];
      document.title = this.board.title + ' | Candidate Tracking System';
      this.setupView();
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getAllVenues() {
    this.subscription = this.candidateService.getAllVenues().subscribe(data => {
      if (data) {
        this.venues = data;
      }
    });
  }

  getAllUsers() {
    this.subscription = this.candidateService.getAllUsers().subscribe(data => {
      if (data) {
        this.users = data;
      }
    });
  }

  setupView() {
    const component = this;
    setTimeout(() => {
      window.addEventListener('resize', (e) => {
        component.updateBoardWidth();
      });
      component.updateBoardWidth();
      document.getElementById('content-wrapper').style.backgroundColor = '';
    }, 100);
  }

  bindPane() {
    const el = document.getElementById('content-wrapper');
    el.addEventListener('mousemove', (e) => {
      e.preventDefault();
      if (curDown === true) {
        el.scrollLeft += (curXPos - e.pageX) * .25; // x > 0 ? x : 0;
        el.scrollTop += (curYPos - e.pageY) * .25; // y > 0 ? y : 0;
      }
    });

    el.addEventListener('mousedown', (e) => {
      if (e.srcElement.id === 'main' || e.srcElement.id === 'content-wrapper') {
        curDown = true;
      }
      curYPos = e.pageY; curXPos = e.pageX;
    });
    el.addEventListener('mouseup', (e) => {
      curDown = false;
    });
  }

  updateBoardWidth() {
    this.boardWidth = ((this.board.columns.length) * 280) + 10;
    if (this.boardWidth > document.body.scrollWidth) {
      document.getElementById('main').style.width = this.boardWidth + 'px';
      document.getElementById('nav-width').style.width = this.boardWidth + 'px';
    } else {
      document.getElementById('main').style.width = '100%';
      document.getElementById('nav-width').style.width = '100%';
    }

    if (this.columnsAdded > 0) {
      const wrapper = document.getElementById('content-wrapper');
      wrapper.scrollLeft = wrapper.scrollWidth;
    }

    this.columnsAdded++;
  }

  updateBoard() {
    if (this.board.title && this.board.title.trim() !== '') {
      // this._boardService.put(this.board);
    } else {
      this.board.title = this.currentTitle;
    }
    this.editingTilte = false;
    document.title = this.board.title + ' | Candidate Tracking System';
  }

  editTitle() {
    this.currentTitle = this.board.title;
    this.editingTilte = true;

    const input = this.el.nativeElement
      .getElementsByClassName('board-title')[0]
      .getElementsByTagName('input')[0];

    setTimeout(() => { input.focus(); }, 0);
  }

  updateColumnElements(column: Column) {
    const columnArr = jQuery('#main .column');
    const columnEl = jQuery('#main .column[columnid=' + column.id + ']');
    let i = 0;
    for (; i < columnArr.length - 1; i++) {
      // tslint:disable-next-line:no-unused-expression
      column.order < +columnArr[i].getAttibute('column-order');
      break;
    }

    columnEl.remove().insertBefore(columnArr[i]);
  }

  updateColumnOrder(event) {
    const i = 0;
    let elBefore = -1;
    let elAfter = -1;
    let newOrder = 0;
    const columnEl = jQuery('#main');
    const columnArr = columnEl.find('.column');

    for (let index = 0; i < columnArr.length - 1; index++) {
      if (columnEl.find('.column')[index].getAttribute('column-id') === event.columnId) {
        break;
      }
    }

    if (i > 0 && i < columnArr.length - 1) {
      elBefore = +columnArr[i - 1].getAttribute('column-order');
      elAfter = +columnArr[i + 1].getAttribute('column-order');

      newOrder = elBefore + ((elAfter - elBefore) / 2);
    } else if (i === columnArr.length - 1) {
      elBefore = +columnArr[i - 1].getAttribute('column-order');
      newOrder = elBefore + 1000;
    } else if (i === 0) {
      elAfter = +columnArr[i + 1].getAttribute('column-order');

      newOrder = elAfter / 2;
    }

    const column = this.board.columns.filter(x => x.id === event.columnId)[0];
    column.order = newOrder;
    // this._columnService.put(column).then(res => {
    //   this._ws.updateColumn(this.board._id, column);
    // });
  }


  blurOnEnter(event) {
    if (event.keyCode === 13) {
      event.target.blur();
    }
  }

  enableAddColumn() {
    this.addingColumn = true;
    const input = jQuery('.add-column')[0]
      .getElementsByTagName('input')[0];

    setTimeout(() => { input.focus(); }, 0);
  }

  addColumn() {
    const newColumn = {
      title: this.addColumnText,
      order: (this.board.columns.length + 1) * 1000,
      boardId: this.board.id
    } as Column;
    // this._columnService.post(newColumn)
    //   .subscribe(column => {
    //     this.board.columns.push(column)
    //     console.log('column added');
    //     this.updateBoardWidth();
    //     this.addColumnText = '';
    //     this._ws.addColumn(this.board._id, column);
    //   });
  }

  addColumnOnEnter(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      if (this.addColumnText && this.addColumnText.trim() !== '') {
        this.addColumn();
      } else {
        this.clearAddColumn();
      }
    } else if (event.keyCode === 27) {
      this.clearAddColumn();
    }
  }

  addColumnOnBlur() {
    if (this.addColumnText && this.addColumnText.trim() !== '') {
      this.addColumn();
    }
    this.clearAddColumn();
  }

  clearAddColumn() {
    this.addingColumn = false;
    this.addColumnText = '';
  }


  addCard(card: Card) {
    this.board.cards.push(card);
  }

  foreceUpdateCards() {
    // var cards = JSON.stringify(this.board.cards);
    // this.board.cards = JSON.parse(cards);
  }


  onCardUpdate(card: Card) {
    this.foreceUpdateCards();
  }

}
