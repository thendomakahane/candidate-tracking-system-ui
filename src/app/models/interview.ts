import { Candidate } from '../models/candidate';
import { User } from '../models/user';
import { Venue } from '../models/venue';
export class Interview {
    id: number;
    dateTime: Date;
    dateObj: any;
    timeObj: {hour: 9, minute: 0};
    candidate: Candidate;
    users: User[];
    venue: Venue;
    isSuccessful: boolean;
    organizer: User;
}
