import { Status } from './status';
export class Column {
    id: Status;
    title: string;
    boardId: string;
    order: number;
}
