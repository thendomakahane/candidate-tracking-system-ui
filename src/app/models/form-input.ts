export class FormInput {
    type: string;
    name: string;
    label: string;
    model: any;
    required: boolean;
    disabled = false;
}
