import { FormInput } from './form-input';
export class ModalConfig {
    title: string;
    bodyText: string;
    button1Text: string;
    button2Text: string;
    size: string;
    formData: FormInput[];
}
