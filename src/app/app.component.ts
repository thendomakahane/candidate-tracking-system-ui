import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from './models/user';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'candidate-traking-system-ui';
  currentUser: User;
  currentUserSubscription: Subscription;

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      console.log('current user', user);
    });
  }

  ngOnDestroy() {
    this.currentUserSubscription.unsubscribe();
  }

  logout() {
    document.getElementById('rightMenu').style.display = 'none';
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  showNavBar() {
    return (this.router.url !== '/login' && !this.router.url.includes('/password-reset'));
  }

  toggleMenu() {
    if (document.getElementById('rightMenu').style.display && document.getElementById('rightMenu').style.display === 'none') {
      document.getElementById('rightMenu').style.display = 'block';
    } else {
      document.getElementById('rightMenu').style.display = 'none';
    }
  }

  navigateToProfile() {
    document.getElementById('rightMenu').style.display = 'none';
    this.router.navigate([`/profile/${this.currentUser.id}`]);
  }

  getUserInitials() {
    if (this.currentUser) {
      return this.currentUser.names.charAt(0) + this.currentUser.surname.charAt(0);
    }
    return '';
  }

  isAdmin() {
    if ( this.currentUser && this.currentUser.role.name.toLowerCase().includes('admin')) {
      return true;
    }
    return false;
  }
}
