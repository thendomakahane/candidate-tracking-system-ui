import { Injectable } from '@angular/core';
import { CandidateService } from './candidate.service';
import { Board } from '../models/board';
import { Column } from '../models/column';
import { Status } from '../models/status';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  boards: Board[] = [];
  constructor(public candidateService: CandidateService) { }

  getAllBoards() {
    const board = new Board();
    board.id = 'candidate-b';
    board.title = 'Recruitment Stages';
    board.columns = this.getAllColumns();
    board.cards = this.candidateService.getCards();
    this.boards.push(board);
    return this.boards;
  }

  getAllColumns(): Column[] {
    const columns = [];
    const telephonicColumn = {
      id: Status.TELEPHONIC_INTERVIEW,
      boardId: 'candidate-b',
      order: 1,
      title: 'Telephonic Interview'
    } as Column;


    const assessmentColumn = {
      id: Status.ASSESSMENT,
      boardId: 'candidate-b',
      order: 2,
      title: 'Assessment'
    } as Column;

    const f2fColumn = {
      id: Status.F2F_INTERVIEW,
      boardId: 'candidate-b',
      order: 3,
      title: 'Face 2 Face Interview'
    } as Column;

    const onboardingColumn = {
      id: Status.ON_BOARDING,
      boardId: 'candidate-b',
      order: 4,
      title: 'On Boarding'
    } as Column;

    const startingColumn = {
      id: Status.PLACEMENTS,
      boardId: 'candidate-b',
      order: 5,
      title: 'Placements'
    } as Column;

    const rejectedColumn = {
      id: Status.REJECTED,
      boardId: 'candidate-b',
      order: 6,
      title: 'Rejected'
    } as Column;


    columns.push(telephonicColumn);
    columns.push(assessmentColumn);
    columns.push(f2fColumn);
    columns.push(onboardingColumn);
    columns.push(startingColumn);
    columns.push(rejectedColumn);
    return columns;
  }
}
