import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CandidateService } from './services/candidate.service';
import { BoardService } from './services/board.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { CardComponent } from './card/card.component';
import { ColumnComponent } from './column/column.component';
import { OrderBy } from './pipes/order-by.pipe';
import { Where } from './pipes/where.pipe';
import { CustomModalComponent } from './custom-modal/custom-modal.component';
import { SearchComponent } from './search/search.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { VenueComponent } from './venue/venue.component';
import { RoleComponent } from './role/role.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { RoleService } from './services/role.service';
import { VenueService } from './services/venue.service';
import { LoginComponent } from './login/login.component';
import { UserService } from './services/user.service';
import { PagerService } from './services/pager.service';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './services/alert.service';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthenticationService } from './services/authentication.service';
import { RolePipe } from './pipes/role.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    CardComponent,
    ColumnComponent,
    OrderBy,
    Where,
    CustomModalComponent,
    SearchComponent,
    AdminComponent,
    UserComponent,
    VenueComponent,
    RoleComponent,
    LoginComponent,
    AlertComponent,
    PasswordResetComponent,
    ProfileComponent,
    RolePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    BrowserAnimationsModule,
    ButtonModule,
    NgbAlertModule,
    TableModule,
    DialogModule,
    InputTextModule,
    ButtonModule,
  ],
  entryComponents: [CustomModalComponent],
  providers: [CandidateService, BoardService, UserService, PagerService, RoleService, VenueService, AlertService, AuthenticationService,
    RoleService, VenueService],
  bootstrap: [AppComponent]
})
export class AppModule { }
