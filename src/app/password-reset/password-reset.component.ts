import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { UserService } from '../services/user.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

  resetForm: FormGroup;
  loading = false;
  submitted = false;
  passwordMissmatch = false;
  userId: string;
  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private alertService: AlertService,
              private userService: UserService, private authenticationService: AuthenticationService) {
    // document.getElementById('nav-width').style.width = '100%';
    this.userId = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      currentPassword: ['', Validators.required],
      newPassword: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  get f() { return this.resetForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.resetForm.invalid) {
        return;
    }

    if (this.f.newPassword.value !== this.f.confirmPassword.value) {
      this.passwordMissmatch = true;
      return;
    }

    this.loading = true;
    const passObj = {
      userId: this.userId,
      password: this.f.newPassword.value
    };
    this.userService.changePassword(JSON.stringify(passObj)).subscribe(user => {
      this.loading = false;
      this.alertService.success('user password successfully updated');
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }, error => {
      this.alertService.error(`Failed to update password ${error}`);
      this.loading = false;
    });
}

}
