import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { AlertService } from '../services/alert.service';
import { phoneNumberValidator } from '../validators/phone-validators';
import { CandidateService } from '../services/candidate.service';
import { Role } from '../models/Role';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  user: User;
  showLoader = false;
  roles: any[];
  constructor(private userService: UserService, private alertService: AlertService,
              private route: ActivatedRoute, private router: Router, private candidateService: CandidateService) {
    document.getElementById('nav-width').style.width = '100%';
    const userId = this.route.snapshot.paramMap.get('id');
    this.showLoader = true;
    this.userService.getUserById(userId).subscribe(user => {
      this.showLoader = false;
      this.user = user;
      this.profileForm = new FormGroup({
        names: new FormControl(this.user.names, [Validators.required, Validators.minLength(2)]),
        surname: new FormControl(this.user.surname, [Validators.required]),
        phoneNumber: new FormControl(this.user.phoneNumber, [Validators.required, phoneNumberValidator, Validators.minLength(10)]),
        gender: new FormControl(this.user.gender, Validators.required),
        email: new FormControl(this.user.email, [Validators.required, Validators.email]),
        role: new FormControl(this.user.role, [Validators.required]),
      });
    }, error => {
      this.showLoader = false;
      this.alertService.error('Unable to retrieve user details, please try again later');
    });
  }

  ngOnInit() {
    this.showLoader = true;
    this.candidateService.getAllRoles().subscribe(data => {
      this.showLoader = false;
      this.roles = data;
    }, error => {
      this.showLoader = false;
      console.log(error);
    });
  }
  compareFn(r1: Role, r2: Role): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  get names() {
    return this.profileForm.get('names');
  }

  get surname() {
    return this.profileForm.get('surname');
  }

  get phoneNumber() {
    return this.profileForm.get('phoneNumber');
  }

  get gender() {
    return this.profileForm.get('gender');
  }

  get email() {
    return this.profileForm.get('email');
  }

  get role() {
    return this.profileForm.get('role');
  }

  onSubmit() {
    this.showLoader = true;
    this.profileForm.value.id = this.user.id;
    this.userService.updateUser(this.profileForm.value).subscribe(data => {
      this.showLoader = false;
      this.alertService.success('User details updated');
    }, error => {
      this.alertService.error('Failed to update user details, please try again later');
      this.showLoader = false;
    });
  }

  changePassword() {
    this.router.navigate([`/password-reset/${this.user.id}`]);
  }

}
