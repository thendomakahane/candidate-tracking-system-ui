import { Rating } from '../models/rating';
import { Interview } from '../models/interview';
import { User } from '../models/user';
export class Evaluation {
    id: number;
    rating: Rating;
    recommendation: string;
    comments: string;
    interview: any;
    user: any;
}
