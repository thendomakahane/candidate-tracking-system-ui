import { Venue } from './../models/venue';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { VenueService } from '../services/venue.service';
import { Alert } from '../models/Alert';


@Component({
  selector: 'app-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.css']
})
export class VenueComponent implements OnInit {

  venueForm: FormGroup;
  alert: Alert;
  showAlert = false;
  venueNameMinLength = 3;
  venueAddressMinLength = 5;
  cols: any;
  venues: Venue[];
  venue: Venue;
  displayDialog;

  constructor(private venueService: VenueService) {
    this.venueForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(this.venueNameMinLength)]),
      address: new FormControl('', [Validators.required, Validators.minLength(this.venueAddressMinLength)]),
    });
  }

  ngOnInit() {

    this.venueService.getAllVenues().subscribe(data => {
      this.venues = data;
      console.log('*** venues ***');
      console.log(data);
    });
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'address', header: 'Address' },
    ];
  }

  clearAll() {
    this.venueForm.reset();
  }

  get name() {
    return this.venueForm.get('name');
  }

  get address() {
    return this.venueForm.get('address');
  }

  onSubmit() {
    this.showAlert = true;
    console.log(this.venueForm.value);
    this.venueService.saveVenue(this.venueForm.value).subscribe(data => {
      console.log('venue saved!!');
      this.alert = {
        type: 'success',
        message: 'SUCCESS: venue saved!'
      };
    }, err => {
      console.error('failed to add user', err);
      this.alert = {
        type: 'danger',
        message: 'ERROR: could not save venue!'
      };
    });
  }

  close() {
    this.showAlert = false;
  }

  showDialogToAdd() {
    this.venue = {
      id: 0,
      name: '',
      address: ''
    };
    this.displayDialog = true;
  }

}
