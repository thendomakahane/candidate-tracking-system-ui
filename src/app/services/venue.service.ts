import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Venue } from '../models/venue';
import { Observable } from 'rxjs';

const HTTP_OPTIONS = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

@Injectable({
  providedIn: 'root'
})
export class VenueService {

  BASE_URL = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  saveVenue(venue: Venue): Observable<any> {
    return this.http.post(this.BASE_URL + 'venue', JSON.stringify(venue), HTTP_OPTIONS);
  }

  getAllVenues(): Observable<Venue[]> {
    return this.http.get<Venue[]>(this.BASE_URL + 'venue', HTTP_OPTIONS);
  }


}
