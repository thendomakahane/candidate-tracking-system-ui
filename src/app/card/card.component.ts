import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ChangeDetectorRef, NgZone } from '@angular/core';
import { Card } from '../models/card';
import { Column } from '../models/column';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() card: Card;
  @Output() cardUpdate: EventEmitter<Card>;
  @Output() attachAffiliation: EventEmitter<Card>;
  @Output() attachEvaluation: EventEmitter<Card>;
  @Output() completeOnBoarding: EventEmitter<Card>;
  @Output() updateCandidate: EventEmitter<Card>;
  @Output() completePlacement: EventEmitter<Card>;
  editingCard = false;
  currentTitle: string;
  zone: NgZone;
  constructor(private el: ElementRef, private ref: ChangeDetectorRef) {
    this.zone = new NgZone({ enableLongStackTrace: false });
    this.cardUpdate = new EventEmitter();
    this.attachAffiliation = new EventEmitter();
    this.attachEvaluation =  new EventEmitter();
    this.completeOnBoarding = new EventEmitter();
    this.updateCandidate = new EventEmitter();
    this.completePlacement =  new EventEmitter();
  }

  ngOnInit() {
  }

  blurOnEnter(event) {
    if (event.keyCode === 13) {
      event.target.blur();
    } else if (event.keyCode === 27) {
      this.card.title = this.currentTitle;
      this.editingCard = false;
    }
  }

  editCard() {
    this.editingCard = true;
    this.currentTitle = this.card.title;

    const textArea = this.el.nativeElement.getElementsByTagName('textarea')[0];

    setTimeout(() => {
      textArea.focus();
    }, 0);
  }

  updateCard() {
    if (!this.card.title || this.card.title.trim() === '') {
      this.card.title = this.currentTitle;
    }

    // this._cardService.put(this.card).then(res => {
    //   this._ws.updateCard(this.card.boardId, this.card);
    // });
    this.editingCard = false;
  }

  onAttachAffiliation() {
    this.attachAffiliation.emit(this.card);
  }

  onAttachEvaluation() {
    this.attachEvaluation.emit(this.card);
  }

  onCompleteOnboarding() {
    this.completeOnBoarding.emit(this.card);
  }

  onUpdateCandidate() {
    this.updateCandidate.emit(this.card);
  }

  onCompletePlacement() {
    this.completePlacement.emit(this.card);
  }

  getStyleClassForRole(name) {
    switch (name) {
      case 'Intern':
        return 'border-left-intern';
      case 'Junior Developer':
        return 'border-left-dev';
      case 'Intermediate Developer':
        return 'border-left-dev';
      case 'Senior Developer':
        return 'border-left-dev';
      case 'Project Manager':
        return 'border-left-pm';
      case 'Solutions Architect':
        return 'border-left-arc';
      case 'Business Analyst':
        return 'border-left-ba';
      case 'Junior Tester':
        return 'border-left-tester';
      case 'Intermediate Tester':
        return 'border-left-tester';
      case 'Senior Tester':
        return 'border-left-tester';
      case 'Scrum Master':
        return 'border-left-scrum';
      case 'HR Manager':
        return 'border-left-hr';
      case 'Recruitment Manager':
        return 'border-left-hr';
      default:
        return 'border-left-default';
    }
  }


}
