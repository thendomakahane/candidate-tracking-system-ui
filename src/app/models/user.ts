export class User {
    id: number;
    names: string;
    surname: string;
    email: string;
    phoneNumber: string;
    role: any;
    gender: string;
    idNumber: string;
    token: string;
    resetPassword: boolean;
}
