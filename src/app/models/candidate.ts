import { Roles } from './roles';
import { Status } from './status';

export class Candidate {
    id: number;
    names: string;
    surname: string;
    email: string;
    phoneNumber: string;
    role: any;
    gender: string;
    status: any;
    idNumber: string;
    assessmentCompleted: boolean;
    evaluationCompleted: boolean;
    onboardingCompleted: boolean;
    placementCompleted: boolean;
}
