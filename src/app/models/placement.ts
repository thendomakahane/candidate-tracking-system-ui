export class Placement {
    id: number;
    scheduledStartDate: Date;
    comment: string;
    candidate: any;
}
