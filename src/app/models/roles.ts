export enum Roles {
    INTERN = 'Intern',
    JUNIOR_DEVELOPER = 'Junior Developer',
    INTERMEDIATE_DEVELOPER = 'Intermediate Developer',
    SENIOR_DEVELOPER = 'Senior Developer',
    PROJECT_MANAGER = 'Project Manager',
    SOLUTIONS_ARCHITECT = 'Solutions Architect',
    BUSINESS_ANALYST = 'Business Analyst',
    JUNIOR_TESTER = 'Junior Tester',
    INTERMEDIATE_TESTER = 'Intyermediate Tester',
    SENIOR_TESTER = 'Senior Tester',
    SCRUM_MASTER = 'Scrum Master',
    HR_MANAGER = 'HR Manager',
    RECRUITMENT_MANAGER = 'Recruitment Manager'
}
