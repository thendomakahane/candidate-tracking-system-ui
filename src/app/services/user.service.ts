import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  BASE_URL = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  saveUser(user: User): Observable<any> {
    return this.http.post(this.BASE_URL + 'user', JSON.stringify(user), HTTP_OPTIONS);
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(this.BASE_URL + 'user', JSON.stringify(user), HTTP_OPTIONS);
  }

  getUserById(id: string): Observable<User> {
    return this.http.get(`${this.BASE_URL}user/${id}`).pipe(map((response: any) => {
      const newUser =  new User();
      newUser.email = response.email;
      newUser.gender = response.gender;
      newUser.id = response.id;
      newUser.idNumber = response.idNumber;
      newUser.role = response.role;
      newUser.surname = response.surname;
      newUser.names = response.names;
      newUser.phoneNumber = response.phoneNumber;
      return newUser;
    }));
  }

  changePassword(passwordObj: any): Observable<User> {
    return this.http.post(`${this.BASE_URL}user/change-password`, passwordObj, HTTP_OPTIONS).pipe(map((response: User) => {
      return response;
    }));
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.BASE_URL + 'user');
  }
}
