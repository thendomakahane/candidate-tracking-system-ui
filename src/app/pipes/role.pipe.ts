import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'role'
})
export class RolePipe implements PipeTransform {

  transform(value: any, col: string): any {
    // debugger;
    if (col === 'role' && value.name) {
      return value.name;
    } else if (col === 'gender' && value.length < 2) {
      if (value === 'M') {
        return 'Male';
      } else if (value === 'F') {
        return 'Female';
      }
    } else {
      return value;
    }
  }

}
