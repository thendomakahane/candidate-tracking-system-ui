import { Component, OnInit, Output, Input, AfterViewInit, EventEmitter, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Card } from '../models/card';
import { Column } from '../models/column';
import { Status } from '../models/status';
import { OrderBy } from '../pipes/order-by.pipe';
import { Where } from '../pipes/where.pipe';
import { ModalConfig } from '../models/modal-config';
import { FormInput } from '../models/form-input';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Candidate } from '../models/candidate';
import { Assessment } from '../models/assessment';
import { CandidateService } from '../services/candidate.service';
import { LookUp } from '../utilities/lookup';
import { CustomModalComponent } from '../custom-modal/custom-modal.component';
import { Interview } from '../models/interview';
import { Evaluation } from '../models/evaluation';
import { Rating } from '../models/rating';
import { Onboarding } from '../models/onboarding';
import { PagerService } from '../services/pager.service';
import { Placement } from '../models/placement';
import { AlertService } from '../services/alert.service';
import * as _ from 'underscore';
import { User } from '../models/user';
import { AuthenticationService } from '../services/authentication.service';

declare var jQuery: any;

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.css'],
  providers: [NgbModalConfig]
})
export class ColumnComponent implements OnInit {
  @Input() column: Column;
  @Input() cards: Card[];
  @Input() roles: any[];
  @Input() statuses: any[];
  @Input() venues: any[];
  @Input() users: any[];
  @Output() addCard: EventEmitter<Card>;
  @Output() cardUpdate: EventEmitter<Card>;

  editingColumn = false;
  addingCard = false;
  addCardText: string;
  currentTitle: string;
  modalConfig: ModalConfig;
  lookUp = new LookUp();
  selectedCard: Card;
  showModal = false;
  showEvalModal = false;
  interview: any;
  meridian = false;
  evaluations = [];
  newEvaluation: Evaluation;
  modalref: NgbModalRef;
  onboardingModel: Onboarding;
  allRatings: any[];
  showLoader = false;
  pager: any = {};
  pagedItems: any[] = [];
  isUpdate = false;
  existingCandidateId: number;
  existingPlacementId: number;
  currentUser: User;
  constructor(private el: ElementRef, private modalService: NgbModal, private ref: ChangeDetectorRef,
              private candidateService: CandidateService, private config: NgbModalConfig, private pagerService: PagerService,
              private alertService: AlertService, private authenticationService: AuthenticationService) {
    this.addCard = new EventEmitter();
    this.cardUpdate = new EventEmitter();
    this.modalConfig = new ModalConfig();
    this.modalConfig.formData = [];
    this.interview = new Interview();
    this.config.backdrop = 'static';
    this.allRatings = this.getRatings();
    this.currentUser = this.authenticationService.currentUserValue;

  }

  ngOnInit() {
    this.venues = [];
    this.users = [];
    this.setupView();
    console.log(this.users);
  }

  onModalOk() {
    this.showModal = false;
  }


  setupView() {
    const component = this;
    let startColumn;
    jQuery('.card-list').sortable({
      connectWith: '.card-list',
      placeholder: 'card-placeholder',
      dropOnEmpty: true,
      tolerance: 'pointer',
      start: (event, ui) => {
        ui.placeholder.height(ui.item.outerHeight());
        startColumn = ui.item.parent().attr('column-id');
      },
      stop: (event, ui) => {
        const senderColumnId = startColumn;
        const targetColumnId = ui.item.closest('.card-list').attr('column-id');
        const cardId = ui.item.find('.card').attr('card-id');
        console.log('stopped dragging', ui);
        if (senderColumnId === targetColumnId) {
          return;
        }

        if (startColumn === '1') {
          const card = this.cards.filter(c => c.id === cardId)[0];
          this.showLoader = true;
          this.candidateService.getAppraisalbyCandidate(card.candidate.id).subscribe(data => {
            // this.showLoader = false;
            if (data.length === 0) {
              window.location.reload();
            } else {
              component.updateCardsOrder({
                columnId: targetColumnId || senderColumnId,
                cardId
              });
            }
          }, er => {
            // this.showLoader = false;
            window.location.reload();
            card.columnId = Status.ASSESSMENT;
            return;
          });
        } else {
          component.updateCardsOrder({
            columnId: targetColumnId || senderColumnId,
            cardId
          });
        }
      }
    });
    jQuery('.card-list').disableSelection();
  }

  compareFn(u1: User, u2: User): boolean {
    return u1 && u2 ? u1.id === u2.id : u1 === u2;
  }

  updateCardsOrder(event) {
    const cardArr = jQuery('[column-id=' + event.columnId + '] .card');
    const i = 0;
    let elBefore = -1;
    let elAfter = -1;
    let newOrder = 0;

    for (let index = 0; i < cardArr.length - 1; index++) {
      if (cardArr[index].getAttribute('card-id') === event.cardId) {
        break;
      }
    }

    if (cardArr.length > 1) {
      if (i > 0 && i < cardArr.length - 1) {
        elBefore = +cardArr[i - 1].getAttribute('card-order');
        elAfter = +cardArr[i + 1].getAttribute('card-order');

        newOrder = elBefore + ((elAfter - elBefore) / 2);
      } else if (i === cardArr.length - 1) {
        elBefore = +cardArr[i - 1].getAttribute('card-order');
        newOrder = elBefore + 1000;
      } else if (i === 0) {
        elAfter = +cardArr[i + 1].getAttribute('card-order');

        newOrder = elAfter / 2;
      }
    } else {
      newOrder = 1000;
    }

    const card = this.cards.filter(x => x.id === event.cardId)[0];
    const oldColumnId = card.columnId;
    card.candidate.status = this.statuses[this.convertToStatus(event.columnId)];
    this.showLoader = true;
    this.candidateService.updateCandidate(card.candidate).subscribe(data => {
      // this.showLoader = false;
      card.candidate.status = data.status;
      card.order = newOrder;
      card.columnId = this.convertToStatus(event.columnId);
      window.location.reload();
    }, errr => {
      // this.showLoader = false;
      card.columnId = oldColumnId;
      window.location.reload();
      console.log(errr);
    });
    // this.ref.detectChanges();

  }

  getRatings() {
    const ratings = [];
    ratings.push({ id: 1, value: 1 });
    ratings.push({ id: 2, value: 2 });
    ratings.push({ id: 3, value: 3 });
    ratings.push({ id: 4, value: 4 });
    ratings.push({ id: 5, value: 5 });
    ratings.push({ id: 6, value: 6 });
    ratings.push({ id: 7, value: 7 });
    return ratings;
  }


  convertToStatus(strRole: string): Status {
    switch (strRole) {
      case '0':
        return Status.TELEPHONIC_INTERVIEW;
      case '1':
        return Status.ASSESSMENT;
      case '2':
        return Status.F2F_INTERVIEW;
      case '3':
        return Status.ON_BOARDING;
      case '4':
        return Status.PLACEMENTS;
      case '5':
        return Status.REJECTED;
      default:
        break;
    }
  }

  addNewCard() {
    this.cards = this.cards || [];
    const newCard = {
      title: this.addCardText,
      order: (this.cards.length + 1) * 1000,
      columnId: this.column.id,
      boardId: this.column.boardId
    } as Card;
  }

  onCardUpdate(card: Card) {
    this.cardUpdate.emit(card);
  }

  isTelephonicInterviewColumn(column: Column) {
    return (column.id === Status.TELEPHONIC_INTERVIEW) ? true : false;
  }

  cardCountPerColumn(column: Column) {
    return this.cards.filter(c => c.columnId === column.id).length;
  }

  onButton1Action() {
    this.modalService.dismissAll();
    this.showModal = false;
  }

  onButton2Action() {
    if (this.modalConfig.title === 'Add Candidate') {
      this.onSave(this.modalConfig.formData);
    } else if (this.modalConfig.title.includes('Assessment Evaluation')) {
      this.onEvaluationComplete(this.modalConfig.formData);
    } else if (this.modalConfig.title.includes('Placement')) {
      this.onPlacementComplete(this.modalConfig.formData);
    }
  }

  onPlacementComplete(formData: FormInput[]) {

    const placement = {
      scheduledStartDate: new Date(formData[0].model.year, formData[0].model.month, formData[0].model.day),
      comment: formData[1].model,
      candidate: this.selectedCard.candidate,
      id: this.existingPlacementId
    };

    this.showLoader = true;
    this.candidateService.addPlacement(placement).subscribe(data => {
      // this.showLoader = false;
      this.modalService.dismissAll();
      this.alertService.success('Placement details saved');
      window.location.reload();
    }, error => {
      // this.showLoader = false;
      this.modalService.dismissAll();
      this.alertService.error('Could not save placement details, please try again later');
      window.location.reload();
      console.log(error);
    });
  }

  onEvaluationComplete(formData: FormInput[]) {
    const givenDate = formData.filter(f => f.name === 'dateGiven')[0].model;
    const submittedDate = formData.filter(f => f.name === 'dateSubmitted')[0].model;
    const appraisal = {
      name: formData.filter(f => f.name === 'name')[0].model,
      dateGiven: new Date(givenDate.year, givenDate.month, givenDate.day),
      dateSubmitted: new Date(submittedDate.year, submittedDate.month, submittedDate.day),
      comments: formData.filter(f => f.name === 'comments')[0].model,
      passed: formData.filter(f => f.name === 'assessmentPassed')[0].model,
      candidate: this.selectedCard.candidate
    };
    this.showLoader = true;
    this.candidateService.addAppraisal(appraisal).subscribe(data => {
      // this.showLoader = false;
      this.modalService.dismissAll();
      this.alertService.success('Assessment details saved');
      window.location.reload();
    }, errr => {
      console.log(errr);
      // this.showLoader = false;
      this.modalService.dismissAll();
      this.alertService.error('Could not save candidate details, please try again later');
      window.location.reload();
    });
  }

  onSave(formData: FormInput[]) {
    const candidate = {
      email: formData.filter(f => f.name === 'email')[0].model,
      names: formData.filter(f => f.name === 'names')[0].model,
      phoneNumber: formData.filter(f => f.name === 'phone-number')[0].model,
      surname: formData.filter(f => f.name === 'surname')[0].model,
      gender: formData.filter(f => f.name === 'gender')[0].model,
      role: formData.filter(f => f.name === 'role')[0].model,
      status: formData.filter(f => f.name === 'stage')[0].model,
      idNumber: formData.filter(f => f.name === 'idNumber')[0].model,
      id: null,
      assessmentCompleted: false,
      evaluationCompleted: false,
      onboardingCompleted: false,
      placementCompleted: false
    } as Candidate;

    this.showLoader = true;
    if (this.isUpdate === true) {
      candidate.id = this.selectedCard.candidate.id;
      candidate.assessmentCompleted = this.selectedCard.candidate.assessmentCompleted;
      candidate.evaluationCompleted = this.selectedCard.candidate.evaluationCompleted;
      candidate.onboardingCompleted = this.selectedCard.candidate.onboardingCompleted;
      candidate.placementCompleted = this.selectedCard.candidate.placementCompleted;
      this.candidateService.updateCandidate(candidate).subscribe(data => {
        // this.showLoader = false;
        this.modalService.dismissAll();
        this.alertService.success('Candidate details updated');
        window.location.reload();
      }, err => {
        // this.showLoader = false;
        console.log('failed to add', err);
        this.modalService.dismissAll();
        this.alertService.error('Failed to update candidate details');
        window.location.reload();
      });
    } else {
      this.candidateService.addCandidate(candidate).subscribe(data => {
        // this.showLoader = false;
        this.modalService.dismissAll();
        this.alertService.success('Candidate added');
        window.location.reload();
      }, err => {
        // this.showLoader = false;
        console.log('failed to add', err);
        this.modalService.dismissAll();
        this.alertService.error('Failed to add candidate, please try again later');
        window.location.reload();
      });
    }

  }

  onSaveInterview() {
    this.interview.dateTime = new Date(this.interview.dateObj.year, this.interview.dateObj.month,
      this.interview.dateObj.day, this.interview.timeObj.hour, this.interview.timeObj.minute);
    this.interview.candidate = this.selectedCard.candidate;
    this.interview.organizer = this.currentUser;
    this.showLoader = true;
    this.candidateService.addInterview(JSON.stringify(this.interview)).subscribe(data => {
      this.interview.id = data;
      this.showLoader = false;
      this.alertService.success('Interview details saved');
      this.candidateService.sendMeetingRequestEmail(JSON.stringify(this.interview)).subscribe(result => {
        console.log(result);
      }, err => {
        console.log(err);
      });
      if (data) {
        this.interview.id = data;
      }
    }, error => {
      this.showLoader = false;
      this.alertService.error('Could not save interview details try again later');
      console.log(error);
    });
  }

  onCloseEvaluation() {
    this.modalService.dismissAll();
    this.showLoader = true;
    window.location.reload();
  }

  onCloseAddEvaluation() {
    this.modalref.close();
  }

  isEvalValid() {
    if (this.newEvaluation.comments && this.newEvaluation.recommendation && this.newEvaluation.rating.abilityToLearn &&
      this.newEvaluation.rating.communicationSkills && this.newEvaluation.rating.flexibility && this.newEvaluation.rating.honesty &&
      this.newEvaluation.rating.logicalSkills && this.newEvaluation.rating.organisationalFit && this.newEvaluation.rating.passion) {
      return true;
    } else {
      return false;
    }
  }

  saveNewEval() {
    this.newEvaluation.interview = this.interview;
    this.newEvaluation.interview.candidate = this.selectedCard.candidate;
    this.onCloseAddEvaluation();
    this.showLoader = true;
    this.candidateService.addEvaluation(JSON.stringify(this.newEvaluation)).subscribe(data => {
      this.showLoader = false;
      this.alertService.success('Evaluation saved');
      // window.location.reload();
      if (data) {
        this.newEvaluation.id = data.id;
        this.evaluations.push(this.newEvaluation);
      }

    }, error => {
      this.showLoader = false;
      this.alertService.error('Failed to save evaluation, please try again later');
      // window.location.reload();
      console.log('failed to add evaluation', error);
    });
  }

  openAssessmentMissingModal() {
    this.modalConfig.title = 'Missing Assessment Evaluation';
    this.modalConfig.formData = [];
    this.modalConfig.bodyText = 'Please attach an assessment evaluation before moving this candidate to next stage';
    this.modalConfig.button1Text = 'Ok';

    const modalRef = this.modalService.open(CustomModalComponent);
    modalRef.componentInstance.modalConfig = this.modalConfig;
    modalRef.componentInstance.button1Action.subscribe(result => {
      this.onButton1Action();
    });
  }

  viewAssessmentEval() {
    this.modalConfig.title = `Assessment Evaluation - ${this.selectedCard.candidate.names} ${this.selectedCard.candidate.surname}`;
    this.modalConfig.formData = [];
    this.modalConfig.button1Text = 'Close';
    const appraisal: Assessment = new Assessment();

    this.showLoader = true;
    this.candidateService.getAppraisalbyCandidate(this.selectedCard.candidate.id).subscribe(data => {
      this.showLoader = false;
      if (data.length === 0) {
        this.openAssessmentEval();
        return;
      }
      this.showLoader = false;
      appraisal.candidate = data[0].candidate;
      appraisal.comments = data[0].comments;
      appraisal.dateGiven = new Date(data[0].dateGiven);
      appraisal.dateSubmitted = new Date(data[0].dateSubmitted);
      appraisal.id = data[0].id;
      appraisal.passed = data[0].passed;
      appraisal.name = data[0].name;

      const name = {
        label: 'Assessment Name',
        model: appraisal.name,
        name: 'name',
        required: true,
        type: 'text',
        disabled: true
      } as FormInput;

      const dateGiven = {
        label: 'Date given',
        model: { year: appraisal.dateGiven.getFullYear(), month: appraisal.dateGiven.getMonth(), day: appraisal.dateGiven.getDate() },
        name: 'dateGiven',
        required: true,
        type: 'date',
        disabled: true
      } as FormInput;

      const dateSubmitted = {
        label: 'Date Submitted',
        model: {
          year: appraisal.dateSubmitted.getFullYear(),
          month: appraisal.dateSubmitted.getMonth(), day: appraisal.dateSubmitted.getDate()
        },
        name: 'dateSubmitted',
        required: true,
        type: 'date',
        disabled: true
      } as FormInput;

      const comments = {
        label: 'Comments',
        model: appraisal.comments,
        name: 'comments',
        required: true,
        type: 'text-area',
        disabled: true
      } as FormInput;

      const passed = {
        label: 'Assessment Passed',
        model: appraisal.passed,
        name: 'assessmentPassed',
        required: true,
        type: 'passed',
        disabled: true
      } as FormInput;

      this.modalConfig.formData.push(name);
      this.modalConfig.formData.push(dateGiven);
      this.modalConfig.formData.push(dateSubmitted);
      this.modalConfig.formData.push(comments);
      this.modalConfig.formData.push(passed);

      const modalRef = this.modalService.open(CustomModalComponent, { size: 'lg' });
      modalRef.componentInstance.modalConfig = this.modalConfig;
      modalRef.componentInstance.button1Action.subscribe(result => {
        this.onButton1Action();
      });
    }, error => {
      this.showLoader = false;
      this.alertService.error('Unable to retrieve assessment details, please try again later');
      console.log(error);
    });
  }

  openPlacementModal(edit, placement = new Placement()) {
    this.modalConfig.title = `Placement Details - ${this.selectedCard.candidate.names} ${this.selectedCard.candidate.surname}`;
    this.modalConfig.formData = [];
    this.modalConfig.button1Text = 'Back';
    this.modalConfig.button2Text = 'Save';

    const startDate = {
      label: 'Scheduled Start Date',
      model: placement.scheduledStartDate,
      name: 'startDate',
      required: true,
      disabled: edit,
      type: 'date'
    } as FormInput;

    const comments = {
      label: 'Comments',
      model: placement.comment,
      name: 'comments',
      required: true,
      type: 'text-area'
    } as FormInput;


    this.modalConfig.formData.push(startDate);
    this.modalConfig.formData.push(comments);

    const modalRef = this.modalService.open(CustomModalComponent, { size: 'lg' });
    modalRef.componentInstance.modalConfig = this.modalConfig;
    modalRef.componentInstance.button1Action.subscribe(result => {
      this.onButton1Action();
    });
    modalRef.componentInstance.button2Action.subscribe(result => {
      this.onButton2Action();
    });

  }

  openAssessmentEval() {
    this.modalConfig.title = `Assessment Evaluation - ${this.selectedCard.candidate.names} ${this.selectedCard.candidate.surname}`;
    this.modalConfig.formData = [];
    this.modalConfig.button1Text = 'Back';
    this.modalConfig.button2Text = 'Save';

    const name = {
      label: 'Assessment Name',
      model: null,
      name: 'name',
      required: true,
      type: 'text'
    } as FormInput;

    const dateGiven = {
      label: 'Date given',
      model: null,
      name: 'dateGiven',
      required: true,
      type: 'date'
    } as FormInput;

    const dateSubmitted = {
      label: 'Date Submitted',
      model: null,
      name: 'dateSubmitted',
      required: true,
      type: 'date'
    } as FormInput;

    const comments = {
      label: 'Comments',
      model: '',
      name: 'comments',
      required: true,
      type: 'text-area'
    } as FormInput;

    const passed = {
      label: 'Assessment Passed',
      model: null,
      name: 'assessmentPassed',
      required: true,
      type: 'passed'
    } as FormInput;

    this.modalConfig.formData.push(name);
    this.modalConfig.formData.push(dateGiven);
    this.modalConfig.formData.push(dateSubmitted);
    this.modalConfig.formData.push(comments);
    this.modalConfig.formData.push(passed);

    const modalRef = this.modalService.open(CustomModalComponent, { size: 'lg' });
    modalRef.componentInstance.modalConfig = this.modalConfig;
    modalRef.componentInstance.button1Action.subscribe(result => {
      this.onButton1Action();
    });
    modalRef.componentInstance.button2Action.subscribe(result => {
      this.onButton2Action();
    });
  }

  onAttachAffiliation(card) {
    this.selectedCard = card;
    if (this.selectedCard.columnId === Status.ASSESSMENT) {
      this.openAssessmentEval();
    } else {
      this.viewAssessmentEval();
    }

  }

  onCompletePlacement(card) {
    this.selectedCard = card;
    this.showLoader = true;
    this.candidateService.getPlacementByCandidate(this.selectedCard.candidate.id).subscribe(data => {
      this.showLoader = false;
      if (data) {
        const startDate = new Date(data.scheduledStartDate);
        this.existingPlacementId = data.id;
        data.scheduledStartDate = { year: startDate.getFullYear(), month: startDate.getMonth(), day: startDate.getDate() };
        this.openPlacementModal(false, data);
      } else {
        this.openPlacementModal(false);
        this.existingPlacementId = null;
      }
    }, error => {
      console.log(error);
      this.alertService.error('Unable to retrieve placement details, please try again later');
      this.showLoader = false;
    });
  }

  onUpdateCandidate(card) {
    this.selectedCard = card;
    let candidate = new Candidate();
    this.showLoader = true;
    this.candidateService.getCandidateById(this.selectedCard.candidate.id).subscribe(data => {
      candidate = data;
      this.showLoader = false;
      candidate.role = this.roles.filter(r => r.id === candidate.role.id)[0];
      candidate.status = this.statuses.filter(s => s.id === candidate.status.id)[0];
      this.openAddCandidateModal(candidate);
    }, error => {
      console.log(error);
      this.alertService.error('Unable to retrieve candidate details, please try again later');
      this.showLoader = false;
    });
  }

  onAttachEvaluation(card, content) {
    this.selectedCard = card;
    this.evaluations = [];
    this.interview = new Interview();
    this.interview.timeObj = { hour: new Date().getHours() + 1, minute: 0 };
    this.modalService.open(content, { size: 'lg' });
    this.showLoader = true;
    this.candidateService.getInterviewByCandidate(this.selectedCard.candidate.id).subscribe(data => {
      if (data) {
        this.interview = data;
        const date = new Date(data.dateTime);
        this.interview.dateObj = { year: date.getFullYear(), month: date.getMonth(), day: date.getDate() };
        this.interview.timeObj = { hour: date.getHours(), minute: date.getMinutes() };
        this.interview.venue = this.venues.filter(v => v.id === data.venue.id)[0];
        this.interview.users.forEach(element => {
          const currentUser = this.users.filter(u => u.id === element.id)[0];
          if (currentUser) {
            element = currentUser;
          }
        });
        this.candidateService.getEvaluationByInterview(this.interview.id).subscribe(result => {
          this.showLoader = false;
          if (result) {
            this.evaluations = result;
            this.setPage(1);
          } else {
            this.pagedItems = [];
          }
        }, error => {
          this.showLoader = false;
          console.log(error);
        });
      } else {
        this.showLoader = false;
      }
    }, error => {
      this.showLoader = false;
      this.alertService.error('Unable to retrieve evaluation details, please try again later');
      console.log(error);
    });
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.evaluations.length, page, 5);

    // get current page of items
    this.pagedItems = this.evaluations.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }


  onCompleteOnboarding(card, content) {
    this.selectedCard = card;
    this.onboardingModel = new Onboarding();
    this.showLoader = true;
    this.candidateService.getOnboardingByCandidate(this.selectedCard.candidate.id).subscribe(data => {
      this.modalService.open(content);
      this.showLoader = false;
      if (data) {
        this.onboardingModel = data;
        console.log(this.onboardingModel);
      }
    }, error => {
      this.showLoader = false;
      this.alertService.error('Unable to retrieve onboarding details, please try again later');
      console.log(error);
    });
  }

  addNewEval(content) {
    this.newEvaluation = new Evaluation();
    this.newEvaluation.rating = new Rating();
    this.modalref = this.modalService.open(content, { size: 'lg', centered: true });
  }

  onCancelOnboarding() {
    this.modalService.dismissAll();
  }

  onSaveOnboarding() {
    this.onboardingModel.candidate = this.selectedCard.candidate;
    this.modalService.dismissAll();
    this.showLoader = true;
    this.candidateService.addOnboarding(JSON.stringify(this.onboardingModel)).subscribe(data => {
      // this.showLoader = false;
      this.alertService.success('Onboarding successful');
      if (data) {
        this.onboardingModel.id = data.id;
      }
      window.location.reload();
    }, error => {
      console.log(error);
      this.alertService.error('could not onboard please try again later');
      // this.showLoader = false;
      window.location.reload();
    });
  }

  onOfferChange(value) {
    this.onboardingModel.offer = value;
  }

  onVerificationChange(value) {
    this.onboardingModel.verification = value;
  }

  onReferenceChange(value) {
    this.onboardingModel.references = value;
  }

  openAddCandidateModal(candidate = new Candidate()) {
    this.modalConfig.title = 'Add Candidate';
    this.modalConfig.button1Text = 'Back';
    this.modalConfig.button2Text = 'Save';
    this.modalConfig.formData = [];

    const names = {
      label: 'Names',
      model: candidate.names,
      name: 'names',
      required: true,
      type: 'text'
    } as FormInput;

    const surname = {
      label: 'Surname',
      model: candidate.surname,
      name: 'surname',
      required: true,
      type: 'text'
    } as FormInput;

    const email = {
      label: 'Email',
      model: candidate.email,
      name: 'email',
      required: true,
      type: 'email'
    } as FormInput;

    const phoneNumber = {
      label: 'Phone Number',
      model: candidate.phoneNumber,
      name: 'phone-number',
      required: true,
      type: 'text'
    } as FormInput;

    const gender = {
      label: 'Gender',
      model: candidate.gender,
      name: 'gender',
      required: true,
      type: 'gender'
    } as FormInput;

    const roles = {
      label: 'Role',
      model: candidate.role,
      name: 'role',
      required: true,
      type: 'role'
    } as FormInput;

    const status = {
      label: 'Stage',
      model: candidate.status,
      name: 'stage',
      required: true,
      type: 'stage'
    } as FormInput;

    const idNumber = {
      label: 'ID Number',
      model: candidate.idNumber,
      name: 'idNumber',
      required: true,
      type: 'text'
    } as FormInput;

    this.modalConfig.formData.push(names);
    this.modalConfig.formData.push(surname);
    this.modalConfig.formData.push(idNumber);
    this.modalConfig.formData.push(email);
    this.modalConfig.formData.push(phoneNumber);
    this.modalConfig.formData.push(gender);
    this.modalConfig.formData.push(roles);
    this.modalConfig.formData.push(status);

    // this.showModal = true;
    const modalRef = this.modalService.open(CustomModalComponent, { size: 'lg' });
    modalRef.componentInstance.modalConfig = this.modalConfig;
    modalRef.componentInstance.roles = this.roles;
    modalRef.componentInstance.statuses = this.statuses;
    if (candidate.id) {
      this.isUpdate = true;
      this.existingCandidateId = candidate.id;
    } else {
      this.isUpdate = false;
      this.existingCandidateId = null;
    }

    modalRef.componentInstance.button1Action.subscribe(result => {
      this.onButton1Action();
    });
    modalRef.componentInstance.button2Action.subscribe(result => {
      this.onButton2Action();
    });
  }

  onModalCancel() {
    this.modalService.dismissAll();
  }

  // Turn enum into array
  toArray(enumme) {
    return Object.keys(enumme)
      .map(key => enumme[key]);
  }
}
