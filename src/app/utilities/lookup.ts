import { Roles } from '../models/roles';
import { Status } from '../models/status';

export class LookUp {
    lookUpRole(roleStr: string) {
        switch (roleStr) {
            case 'Intern':
                return Roles.INTERN;
            case 'Junior Developer':
                return Roles.JUNIOR_DEVELOPER;
            case 'Intermediate Developer':
                return Roles.INTERMEDIATE_DEVELOPER;
            case 'Senior Developer':
                return Roles.SENIOR_DEVELOPER;
            case 'Project Manager':
                return Roles.PROJECT_MANAGER;
            case 'Solutions Architec':
                return Roles.SOLUTIONS_ARCHITECT;
            case 'Business Analyst':
                return Roles.BUSINESS_ANALYST;
            case 'Junior Tester':
                return Roles.JUNIOR_TESTER;
            case 'Intermediate Tester':
                return Roles.INTERMEDIATE_TESTER;
            case 'Senior Tester':
                return Roles.SENIOR_TESTER;
            case 'Scrum Master':
                return Roles.SCRUM_MASTER;
            case 'HR Manager':
                return Roles.HR_MANAGER;
            case 'Recruitment Manager':
                return Roles.RECRUITMENT_MANAGER;
            default:
                break;
        }
    }

    lookUpStatus(statusStr: string) {
        switch (statusStr) {
            case 'Telephonic Interview':
                return Status.TELEPHONIC_INTERVIEW;
            case 'Assessment':
                return Status.ASSESSMENT;
            case 'F2F Interview':
                return Status.F2F_INTERVIEW;
            case 'On Boarding':
                return Status.ON_BOARDING;
            case 'Placements':
                return Status.PLACEMENTS;
            case 'Rejected':
                return Status.REJECTED;
            default:
                break;
        }
    }
}
