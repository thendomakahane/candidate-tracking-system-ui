import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CandidateService } from '../services/candidate.service';
import { UserService } from '../services/user.service';
import { Alert } from '../models/Alert';
import { phoneNumberValidator } from '../validators/phone-validators';
import { User } from '../models/user';
import { Role } from '../models/Role';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  roles: any[];
  userForm: FormGroup;
  alert: Alert;
  showAlert = false;
  users: User[];
  displayDialog: boolean;
  cols: any[];
  namesMinLength = 3;
  phoneMinLength = 10;
  user: User;

  constructor(private candidateService: CandidateService,
              private userService: UserService) {
    this.userForm = new FormGroup({
      names: new FormControl('', [Validators.required, Validators.minLength(this.namesMinLength)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(this.namesMinLength)]),
      phoneNumber: new FormControl('', [Validators.required, phoneNumberValidator, Validators.minLength(this.phoneMinLength)]),
      gender: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      role: new FormControl( '', Validators.required),
    });
  }

  ngOnInit() {
    this.candidateService.getAllRoles().subscribe(data => {
      this.roles = data;
    }, error => {
      console.log(error);
    });

    this.userService.getAllUsers().subscribe(data => {
      this.users = data;
    }, error => {
      console.log(error);
    });

    this.cols = [
      { field: 'names', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'gender', header: 'Gender' },
      { field: 'phoneNumber', header: 'Phone Number' },
      { field: 'role', header: 'Role' },
      { field: 'email', header: 'Email' }
  ];
  }

  clearAll() {
    this.userForm.reset();
  }

  onSubmit() {
    this.showAlert = true;
    console.log(this.userForm.value);
    this.userService.saveUser(this.userForm.value).subscribe(data => {
      console.log('saved user');
      this.alert = {
        type: 'success',
        message: 'SUCCESS: user saved!'
      };
    }, err => {
      console.error('failed to add user', err);
      this.alert = {
        type: 'danger',
        message: 'ERROR: could not save user!'
      };
    });
  }

  showDialogToAdd() {
    this.user = {
      id: 0,
      names: '',
      surname: '',
      email: '',
      phoneNumber: '',
      role: null,
      gender: '',
      idNumber: '',
      token: '',
      resetPassword: false,
    };
    this.displayDialog = true;
  }

  close() {
    this.showAlert = false;
  }

  get names() {
    return this.userForm.get('names');
  }

  get surname() {
    return this.userForm.get('surname');
  }

  get phoneNumber() {
    return this.userForm.get('phoneNumber');
  }

  get gender() {
    return this.userForm.get('gender');
  }

  get email() {
    return this.userForm.get('email');
  }

  get role() {
    return this.userForm.get('role');
  }

}
